from core.baseplugin import BasePlugin
from core.menuitem import MenuItem
from core.page import Page


class Plugin(BasePlugin):
    def __init__(self):
        super(Plugin, self).__init__()
        self.register_menu_item(MenuItem(self.strings.Backup, 'fas fa-save', "/plugins/backup/"))

        self.register_page(Page('', Page.METHOD_GET, self.action_index))

    def action_index(self, msg_domain=None, msg_add=None):
        return self.render("index", {"name": "backupmanager"})
