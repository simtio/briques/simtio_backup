# SIMTIO - BackupManager

This container contain BackupManager. Used as a backup solution.

## Starting the container & Configuring
```
docker run -d -v stuff-to-backups:/mnt/backups -v backups-storage:/mnt/archives --name=simtio-backupmanager simtio/backupmanager
```

## Environment Parameters
|Name|Default|Use|
|-|-|-|
|PUID|1000|App user uid - See below|
|PGID|1000|App user gid - See below|
|CRON|`0 2 * * *`|Backup run time|
|ARCHIVES_PATH|`/mnt/archives`|Destination repository for compressed backups|
|ARCHIVES_TTL|`7`|Archives Time-To-Live|
|ARCHIVE_METHOD|`tarball`|Available methods (coma separated): `tarball,tarball-incremental,mysql,pgsql,svn,pipe,none`|
|TARBALL_FILETYPE|'tar.bz2'|Available methods: `tar, tar.gz, tar.bz2`|
|TARBALL_DIRECTORIES|`/mnt/backups`|Directories to backup (space separated)|

When using volumes (-v flags) permissions issues can arise between the host OS and the container, we avoid this issue by allowing you to specify the user PUID and group PGID.

Ensure any volume directories on the host are owned by the same user you specify and any permissions issues will vanish like magic.

## Dev
```
# Dockerhub
docker build -t simtio/backupmanager:latest -t simtio/backupmanager:0.x .
docker push simtio/backupmanager:latest && docker push simtio/backupmanager:0.x

docker buildx build -t simtio/backupmanager:latest -t simtio/backupmanager:0.x --platform=linux/aarch64,linux/amd64,linux/arm . --push
```

## Sources
- https://github.com/linuxserver/docker-backupmanager
- https://github.com/backupmanager/backupmanager/wiki/Headless-installation-on-Debian-or-Ubuntu
