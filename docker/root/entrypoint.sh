#!/usr/bin/env bash

# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail

# Set path
PATH=/bin:/sbin:/usr/bin:/usr/sbin

############
## CONFIG ##
############
# Setup backupmanager user
PUID=${PUID:-1000}
PGID=${PGID:-1000}
groupmod -o -g "$PGID" backupmanager
usermod -o -u "$PUID" backupmanager
echo '
--------------------------------------------
  ___   ___   __  __   _____   ___    ___
 / __| |_ _| |  \/  | |_   _| |_ _|  / _ \
 \__ \  | |  | |\/| |   | |    | |  | (_) |
 |___/ |___| |_|  |_|   |_|   |___|  \___/

--------------------------------------------

Brought to you by SIMTIO
https://www.simtio.fr
--------------------------------------------
GID/UID
--------------------------------------------'
echo "
User uid:    $(id -u backupmanager)
User gid:    $(id -g backupmanager)
--------------------------------------------
"

# Container variables
CRON=${CRON:-'0 2 * * *'}
ARCHIVES_PATH=${ARCHIVES_PATH:-'/mnt/archives'}
ARCHIVES_TTL=${ARCHIVES_TTL:-'7'}
ARCHIVE_METHOD=${ARCHIVE_METHOD:-'tarball'}
TARBALL_FILETYPE=${TARBALL_FILETYPE:-'tar.bz2'}
TARBALL_DIRECTORIES=${TARBALL_DIRECTORIES:-'/mnt/backups'}

# Setup
mkdir -pv ${ARCHIVES_PATH} ${TARBALL_DIRECTORIES}
chown -Rv backupmanager:backupmanager ${ARCHIVES_PATH} ${TARBALL_DIRECTORIES}
echo "${CRON} root /backup-manager.sh" > /cron-backupmanager

# Set trap to stop the script proprely when a docker stop is executed
trap : EXIT TERM KILL INT SIGKILL SIGTERM SIGQUIT

# Launch cron
crontab /cron-backupmanager
cron -f
